Day06-assess the quality of your assembly compared to the course assembly, examine SAM files, start genotyping:
###Make sure to add all this to your logfile as you go, including the output from the various scripts/greps/etc.
###Assembly quality assessment (following the first two steps in the recommended trinity assessment protocol from https://github.com/trinityrnaseq/trinityrnaseq/wiki/Transcriptome-Assembly-Quality-Assessment)
1- start an interactive session via salloc and run the /cm/shared/apps/trinity/2.0.6/util/TrinityStats.pl script on your Trinity.fasta output from your assembly
2- compare this with the output from avg_cov_len_fasta_advbioinf.py on our class reference assembly (/cm/shared/courses/dbarshis/21AdvGenomics/classdata/Astrangia_poculata/refassembly/15079_Apoc_hostsym.fasta) and add both to your logfile
3- less or head your bowtie2 job output file to look at your alignment statistics and calculate the following from the information:
	a-the mean percent "overall alignment rate"
	b-the mean percent reads "aligned exactly 1 time"
	c-the mean number of reads "aligned exactly 1 time"
	d-the mean percent reads "aligned >1 times"
	
	hint use grep and paste into excel
4- add your statistics as single rows to the shared table /cm/shared/courses/dbarshis/21AdvGenomics/classdata/Astrangia_poculata/alignmentstatstable.txt as tab-delimited text in the following order:
LaneX_yourinitials	b-the mean percent "overall alignment rate"	c-the mean percent reads "aligned exactly 1 time"	d-the mean number of reads "aligned exactly 1 time"	e-the mean percent reads "aligned >1 times"
5- Data cleanup and archiving:
	a-mv your Trinity.fasta file from your trinity_out_dir to a folder called testassembly in your data directory
	b-set up a sbatch script to:
		rm -r YOURtrinity_out_dir
		rm -r YOURoriginalfastqs
		rm -r YOURfilteringstats # as long as you've already appended your filteringstats output to the class table as part of homework_day04.txt

6- submit a blast against sprot from your testassembly folder using the following command
#!/bin/bash -l

#SBATCH -o OUTFILENAME.txt
#SBATCH -n 6         
#SBATCH --mail-user=EMAIL
#SBATCH --mail-type=END
#SBATCH --job-name=JOBNAME

enable_lmod
module load container_env blast
blastx -query Trinity.fasta -db /cm/shared/apps/blast/databases/uniprot_sprot_Sep2018 -out blastx.outfmt6 \
        -evalue 1e-20 -num_threads 6 -max_target_seqs 1 -outfmt 6
###Exploring our SAM files, check out http://bowtie-bio.sourceforge.net/bowtie2/manual.shtml#sam-output for bowtie2 specific output and http://bio-bwa.sourceforge.net/bwa.shtml#4 for general SAM output
7- head one of your .sam files to look at the header
8- grep -v '@' your.sam | head to look at the sequence read lines, why does this work to exclude the header lines?
9- in an interactive session run /cm/shared/courses/dbarshis/21AdvGenomics/scripts/get_explain_sam_flags_advbioinf.py on 2-3 of your .sam files using * to select 2-3 at the same time.
10- we need to run the read sorting step required for SNP calling, so if you have time, set up and run the following script on your .sam files to finish before Wednesday:
#!/bin/bash -l

#SBATCH -o OUTFILENAME.txt
#SBATCH -n 1         
#SBATCH --mail-user=EMAIL
#SBATCH --mail-type=END
#SBATCH --job-name=JOBNAME

enable_lmod
module load samtools/1
for i in *.sam; do `samtools view -bS $i > ${i%.sam}_UNSORTED.bam`; done

for i in *UNSORTED.bam; do samtools sort $i > ${i%_UNSORTED.bam}.bam
samtools index ${i%_UNSORTED.bam}.bam
done
