Homework day03 (document all this on your github page, don't forget to pwd before each entry to remind yourself where you were working):
0.5- Finish HW/exercises from day02 if you didn't already in class
1. cp the /cm/shared/courses/dbarshis/21AdvGenomics/assignments_exercises/day03 directory (and files) to your sandbox
2. mkdir a fastq directory in your data directory and mv all the .fastq files into this directory
3. cp the renamingtable_complete.txt from the day03 directory into your fastq directory and the /cm/shared/courses/dbarshis/21AdvGenomics/scripts/renamer_advbioinf.py script into your sandbox scripts folder and less the new script and check out the usage statement
4. run the renamer_advbioinf.py script in your fastq folder using the renamingtable_complete.txt as practice and verify the output to the screen by hand
5. Uncomment the last line of the renaming script in your scripts folder that starts with os.popen and comment out the next to last line that starts with print
6. write a sbatch script and submit it to rename all the .fastq files according to the renaming table using your renamer_advbioinf.py script
7. Make sure this is all documented on your github page
8. The naming convention for the files is as follows:
  * SOURCEPOPULATION_SYMBIOTICSTATE_GENOTYPE_TEMPERATURE.fastq
  * There are 2 sources: Virginia and Rhode Island
  * There are 2 symbiotic states: Brown and White
9. Next, you're going to start the process of adapter clipping and quality trimming all the renamed .fastq files in batches, by lane
10. cp the script /cm/shared/courses/dbarshis/21AdvGenomics/scripts/Trimclipfilterstatsbatch_advbioinf.py into your scripts directory
11. Less/head the new script and check out the usage statement
12. cp the /cm/shared/courses/dbarshis/21AdvGenomics/assignments_exercises/day03/adapterlist_advbioinf.txt into the working directory with your fastq files
13. Make a sbatch script for the Trimclipfilter... script and run it on your fastq files
14. This will take a while (like days)
15. Now might be a good time to update everything on your github
