Exercise day02:
1- Logon to the cluster @turing.hpc.odu.edu
2- Make a directory in your course workspace called data
3- Make a directory called exercises in your data directory
4- execute a pwd command and start a log of your commands with a header for today's date in your README.md github logfile in your workspace
5- cp the Exercise2.fasta.gz and Exercise2.fastq.tar.gz files into your exercises directory from the /cm/shared/courses/dbarshis/21AdvGenomics/assignments_exercises/day02 directory
6- unzip and untar the files
7- add these commands to your notebook file
8- calculate how many sequences are in each file and add these results to your notebook file
9- cp the avg_cov_len_fasta_advbioinf.py from the /cm/shared/courses/dbarshis/21AdvGenomics/scripts directory into your class scripts directory
10- start an interactive compute session and re-navigate to your exercises directory
11- run the avg_cov_len_fasta_DJB.py script on your Exercise2.fasta file by typing the path to the script followed by the Exercise2.fasta file name
12- did you add all these entries to your notebook file, including the results of the script?
13- push your notebook file to your github page
