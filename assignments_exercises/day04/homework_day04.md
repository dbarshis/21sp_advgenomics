## Homework day04 (document all this in your notebook, don't forget to pwd before each entry to remind yourself where you were working):
1. Add your trimclipstats.txt output to the full class datafile /cm/shared/courses/dbarshis/21AdvGenomics/classdata/Astrangia_poculata/Fulltrimclipstatstable.txt using the following steps

  * 1a run `/cm/shared/courses/dbarshis/21AdvGenomics/scripts/Schafran_trimstatstable_advbioinf_clippedtrimmed.py -h` to examine the usage

  * 1b-Run the script on your data with the outputfilename YOURNAME_trimclipstatsout\.txt

  * 1c-Add YOURNAME_trimclipstatsout\.txt to the class file by running

    ```tail -n +2 YOURNAME_trimclipstatsout.txt >> /cm/shared/courses/dbarshis/21AdvGenomics/classdata/Astrangia_poculata/Fulltrimclipstatstable.txt```


2. Now we're going to map our reads back to our assembly using the bowtie2 alignment algorithm (starting to follow this pipeline https://github.com/bethsheets/SNPcalling_tutorial)

3. write a sbatch script to do the following commands in sequence on your _clippedtrimmed.fastq datafiles from your lane of data

``` sh
module load bowtie2/2.4
for i in *_clippedtrimmed.fastq; do bowtie2 --rg-id ${i%_clippedtrimmed.fastq} \
--rg SM:${i%_clippedtrimmed.fastq} \
--very-sensitive -x /cm/shared/courses/dbarshis/21AdvGenomics/classdata/Astrangia_poculata/refassembly/Apoc_hostsym -U $i \
> ${i%_clippedtrimmedfilterd.fastq}.sam --no-unal -k 5; done
```

4. Submit and add everything to your logfile

testing for git pull again